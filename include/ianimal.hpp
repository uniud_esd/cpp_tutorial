#ifndef IANIMAL_HPP
#define IANIMAL_HPP

#include <string>
#include "feeling.hpp"

using namespace std;

struct IAnimal {
    
  virtual const unsigned int getAge() const = 0;
  virtual void setAge(unsigned int) = 0;

  virtual Feeling feelsAbout(IAnimal&) const = 0;
};

#endif
