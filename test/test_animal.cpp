#include <string>
#include <iostream>
#include "animal.hpp"
#include "cat.hpp"
#include "dog.hpp"

using namespace std;

int main(int argc, char** argv) {

  string firstRace(argv[1]);
  string secondRace(argv[2]);

  Animal* first;
  Animal* second;
  
  if (firstRace == "cat")
    first = new Cat(4);
  else if (firstRace == "dog")
    first = new Dog(4);

  if (secondRace == "cat")
    second = new Cat(4);
  else if (secondRace == "dog")
    second = new Dog(4);

  cout << toString(first->feelsAbout(*second));

  return 0;
}
