#include "dog.hpp"
#include "cat.hpp"

Dog::Dog(unsigned int age) : Animal(age)
{
}

Feeling Dog::feelsAbout(IAnimal& other) const {

  Feeling result;

  Cat* cat_ptr = dynamic_cast<Cat*>(&other);
  Dog* dog_ptr = dynamic_cast<Dog*>(&other);
  
  if (cat_ptr)
      result = Feeling::HATRED;
  else if (dog_ptr) 
      result = Feeling::CURIOSITY;
  else
      result = Feeling::INDIFFERENCE;

  return result;
}
