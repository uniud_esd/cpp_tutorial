template<class T, unsigned S>
class Mux {
  private:
    const T* values;
  public:
    Mux(const T* val) : values(val) { }
    T select(const unsigned index) const { 
        if (index >= S) throw;
        return values[index];
    }
};


