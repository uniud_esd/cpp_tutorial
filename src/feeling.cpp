#include "feeling.hpp"

string toString(Feeling feeling) {
  
  string result;
  
  switch (feeling) {
    case Feeling::HATRED:
      result = "hatred";
      break;
    case Feeling::INDIFFERENCE:
      result = "indifference";
      break;
    case Feeling::FEAR:
      result = "fear";
      break;
    case Feeling::CURIOSITY:
      result = "curiosity";
  }
  
  return result;
}