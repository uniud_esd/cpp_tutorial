#include <string>
#include "animal.hpp"

Animal::Animal(unsigned int age) {

  this->age = age;
}

const unsigned int Animal::getAge() const {
  return this->age;
}

void Animal::setAge(unsigned int age) {
  this->age = age;
}
