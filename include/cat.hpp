#ifndef CAT_HPP
#define CAT_HPP

#include "animal.hpp"

using namespace std;

class Cat : public Animal {

  public:

    Cat(unsigned int age);

    virtual Feeling feelsAbout(IAnimal&) const;
};

#endif
