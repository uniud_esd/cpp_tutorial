#ifndef DOG_HPP
#define DOG_HPP

#include "animal.hpp"

using namespace std;

class Dog : public Animal {

  public:

    Dog(unsigned int age);

    virtual Feeling feelsAbout(IAnimal&) const;
};

#endif
