#ifndef ANIMAL_HPP
#define ANIMAL_HPP

#include <string>
#include "ianimal.hpp"

using namespace std;

class Animal : public IAnimal {

  private:

    unsigned int age;

  public:

    Animal(unsigned int);
    
    const unsigned int getAge() const;
    void setAge(unsigned int);

    virtual Feeling feelsAbout(IAnimal&) const = 0;
};

#endif
