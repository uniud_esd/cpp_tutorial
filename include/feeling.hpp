#ifndef FEELING_HPP
#define FEELING_HPP

#include <string>

using namespace std;

enum class Feeling { HATRED, INDIFFERENCE, FEAR, CURIOSITY };

string toString(Feeling);

#endif