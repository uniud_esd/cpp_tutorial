#include "cat.hpp"
#include "dog.hpp"

Cat::Cat(unsigned int age) : Animal(age)
{
}

Feeling Cat::feelsAbout(IAnimal& other) const {

  Feeling result;

  Cat* cat_ptr = dynamic_cast<Cat*>(&other);
  Dog* dog_ptr = dynamic_cast<Dog*>(&other);
  
  if (cat_ptr)
      result = Feeling::INDIFFERENCE;
  else if (dog_ptr) 
      result = Feeling::FEAR;
  else
      result = Feeling::INDIFFERENCE;

  return result;
}
