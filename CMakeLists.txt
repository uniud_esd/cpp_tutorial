cmake_minimum_required (VERSION 2.6)

include_directories ("include")

add_library (animal "src/animal.cpp" "src/dog.cpp" "src/cat.cpp" "src/feeling.cpp")

add_executable (test_animal test/test_animal.cpp)
add_executable (test_mux test/test_mux.cpp)

set_target_properties(animal PROPERTIES COMPILE_FLAGS "-std=c++11")
set_target_properties(test_animal PROPERTIES COMPILE_FLAGS "-std=c++11")

target_link_libraries (test_animal animal)

enable_testing ()

add_test (Fears test_animal cat dog)
add_test (Hates test_animal dog cat) 
add_test (Mux test_mux)

set_tests_properties (Fears PROPERTIES PASS_REGULAR_EXPRESSION "fear")
set_tests_properties (Hates PROPERTIES PASS_REGULAR_EXPRESSION "hatred")

