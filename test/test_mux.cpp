#include <string>
#include <iostream>
#include <assert.h>
#include "mux.hpp"

using namespace std;

int main(int argc, char** argv) {

  const unsigned INT_SIZE = 3;
  const unsigned STRING_SIZE = 2;

  bool error = false;

  int* intValues = new int[INT_SIZE];
  intValues[0] = 5;
  intValues[1] = 1;
  intValues[2] = -2;

  Mux<int,INT_SIZE> intMux(intValues);

  for (unsigned i = 0; i < INT_SIZE; i++)
    error |= (intMux.select(i) != intValues[i]);

  string* stringValues = new string[STRING_SIZE];
  stringValues[0] = "one";
  stringValues[1] = "two";

  Mux<string,STRING_SIZE> stringMux(stringValues);

  for (unsigned i = 0; i < STRING_SIZE; i++)
    error |= (stringMux.select(i).compare(stringValues[i]) != 0);

  return error;
}
